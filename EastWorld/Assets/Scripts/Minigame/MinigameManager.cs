﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

/*
 * The MinigameManager runs server only and handles the collection
 * of minigames and the players that join/leave the game start trigger.
 */
public class MinigameManager : MonoBehaviour
{
    public static MinigameManager singleton;

    // Registry of all minigames. Needed to send a reference of an minigame over
    // the network (using the list offset)
    private List<Minigame> minigameRegistry;
    public List<Minigame> MinigameRegistry
    {
        get { return minigameRegistry; }
        set { minigameRegistry = value; }
    }

    // Map of colliders and there corresponding minigames
    private Dictionary<Collider, Minigame> minigameTriggers;
    public Dictionary<Collider, Minigame> MinigameTriggers
    {
        get { return minigameTriggers; }
        set { minigameTriggers = value; }
    }

    // Map of minigame references and all registered players in this trigger
    private Dictionary<Minigame, List<Player>> registeredPlayers;
    public Dictionary<Minigame, List<Player>> RegisteredPlayers
    {
        get { return registeredPlayers; }
        set { registeredPlayers = value; }
    }

    // List of running minigames
    private List<Minigame> runningMinigames;
    private Queue<Minigame> minigamesToStart;
    private Queue<Minigame> minigamesToStop;

    public MinigameManager()
    {
        singleton = this;
        minigameRegistry = new List<Minigame>();
        minigameTriggers = new Dictionary<Collider, Minigame>();
        registeredPlayers = new Dictionary<Minigame, List<Player>>();
        runningMinigames = new List<Minigame>();
        minigamesToStart = new Queue<Minigame>();
        minigamesToStop = new Queue<Minigame>();
    }

    // Register a new minigame to the MinigameManager
	public void RegisterMinigame(Collider collider, Minigame minigame)
	{
        minigameRegistry.Add(minigame);
		minigameTriggers.Add(collider, minigame);
		registeredPlayers.Add(minigame, new List<Player>());
	}

    // Message to start/stop a minigame from a client
    public class MinigameMessage : MessageBase
    {
        public Minigame minigame;

        public override void Serialize(NetworkWriter writer)
        {
            writer.Write(MinigameManager.singleton.MinigameRegistry.IndexOf(minigame));
        }

        public override void Deserialize(NetworkReader reader)
        {
            minigame = MinigameManager.singleton.MinigameRegistry[reader.ReadInt32()];
        }
    }

    // Queue up a minigame to be started, return false if the minigame is already running
    public bool StartMinigame(Minigame minigame)
    {
        if (runningMinigames.Contains(minigame))
            return false;

        minigamesToStart.Enqueue(minigame);
        return true;
    }

    public void StartMinigame(NetworkMessage networkMessage)
    {
        MinigameMessage message = networkMessage.ReadMessage<MinigameMessage>();
        StartMinigame(message.minigame);
    }

    // Queue up a minigame to be stoped, return false if the minigame is not running
    public bool StopMinigame(Minigame minigame)
    {
        if (!runningMinigames.Contains(minigame))
            return false;

        minigamesToStop.Enqueue(minigame);
        return true;
    }

    public void StopMinigame(NetworkMessage networkMessage)
    {
        MinigameMessage message = networkMessage.ReadMessage<MinigameMessage>();
        StopMinigame(message.minigame);
    }

    public class PlayerRegistrationMessage : MessageBase
    {
        public Minigame minigame;
        public NetworkInstanceId id;

        public override void Serialize(NetworkWriter networkWriter)
        {
            networkWriter.Write(MinigameManager.singleton.MinigameRegistry.IndexOf(minigame));
            networkWriter.Write(id);
        }

        public override void Deserialize(NetworkReader networkReader)
        {
            minigame = MinigameManager.singleton.MinigameRegistry[networkReader.ReadInt32()];
            id = networkReader.ReadNetworkId();
        }
    }

    private UILobby.UIMessage GenerateMesage(Minigame minigame)
    {
        UILobby.UIMessage uiMessage = new UILobby.UIMessage();
        uiMessage.minigame = minigame;
        List<Player> playerList = registeredPlayers[minigame];
        uiMessage.playerNames = new string[playerList.Count];

        for (int i = 0; i < uiMessage.playerNames.Length; i++)
            uiMessage.playerNames[i] = playerList[i].PlayerName;

        return uiMessage;
    }

    // Register a player to a minigame
    public void RegisterPlayer(PlayerRegistrationMessage message)
    {
        Player player = NetworkServer.FindLocalObject(message.id).GetComponent<Player>();
        if (player == null)
            return;

        List<Player> playerList;
        if (!registeredPlayers.TryGetValue(message.minigame, out playerList))
            return;

        playerList.Add(player);

        // Update UI
        UILobby.UIMessage uiMessage = GenerateMesage(message.minigame);
        foreach (Player p in playerList)
            p.connectionToClient.Send(201, uiMessage);
    }

    // Wrapper for Register with NetworkMessage
    public void RegisterPlayer(NetworkMessage networkMessage)
    {
        RegisterPlayer(networkMessage.ReadMessage<PlayerRegistrationMessage>());
    }

    // Unregister a player from a minigame
    public void UnregisterPlayer(PlayerRegistrationMessage message)
    {
        Player player = NetworkServer.FindLocalObject(message.id).GetComponent<Player>();
        if (player == null)
            return;

        List<Player> playerList;
        if (!registeredPlayers.TryGetValue(message.minigame, out playerList))
            return;

				if(!playerList.Contains(player))
				{
					player.connectionToClient.Send(202, new EmptyMessage());
					return;
				}

				playerList.Remove(player);

        player.connectionToClient.Send(202, new EmptyMessage());

        UILobby.UIMessage uiMessage = GenerateMesage(message.minigame);
        foreach (Player p in playerList)
            p.connectionToClient.Send(201, uiMessage);
    }

    // Wrapper for Unregister with NetworkMessage
    public void UnregisterPlayer(NetworkMessage networkMessage)
    {
        UnregisterPlayer(networkMessage.ReadMessage<PlayerRegistrationMessage>());
    }

    private void Update()
    {
        // Stop minigames
        for (int i = 0; i < minigamesToStop.Count; i++)
        {
            Minigame minigame = minigamesToStop.Dequeue();
            runningMinigames.Remove(minigame);
            minigame.GameStopHandler();
        }

        // Start minigames
        for (int i = 0; i < minigamesToStart.Count; i++)
        {
            Minigame minigame = minigamesToStart.Dequeue();
            runningMinigames.Add(minigame);
            minigame.GameStartHandler();
        }

        // Call all functions subscribed to the GameUpdateHandler of each minigame
        foreach (Minigame minigame in runningMinigames)
            minigame.GameUpdateHandler();
    }

		public bool IsRunning(Minigame minigame)
		{
			if(runningMinigames.Contains(minigame))
				return true;

			return false;
		}
}
