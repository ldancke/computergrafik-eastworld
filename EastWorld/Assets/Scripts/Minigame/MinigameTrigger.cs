﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Every MinigameTrigger has one corresponding minigame.
 * Also communicates with the manager about whom is in the trigger.
 */
[RequireComponent(typeof(Collider))]
public class MinigameTrigger : MonoBehaviour
{
    [SerializeField]
    private Minigame minigame;

    private void Awake()
    {
        MinigameManager.singleton.RegisterMinigame(GetComponent<Collider>(), minigame);
    }

    private void OnTriggerEnter(Collider other)
    {
			if(MinigameManager.singleton.IsRunning(minigame))
				return;

      Player player = other.GetComponent<Player>();
      if (player == null)
          return;

      if (!player.isLocalPlayer)
          return;

      if (!player.isServer)
          player.connectionToServer.Send(101, new MinigameManager.PlayerRegistrationMessage { minigame = this.minigame, id = player.netId });
      else
          MinigameManager.singleton.RegisterPlayer(new MinigameManager.PlayerRegistrationMessage{ minigame = this.minigame, id = player.netId });
    }

    private void OnTriggerExit(Collider other)
    {
      Player player = other.GetComponent<Player>();
      if (player == null)
          return;

      if (!player.isLocalPlayer)
          return;

      if (!player.isServer)
          player.connectionToServer.Send(102, new MinigameManager.PlayerRegistrationMessage { minigame = this.minigame, id = player.netId });
      else
          MinigameManager.singleton.UnregisterPlayer(new MinigameManager.PlayerRegistrationMessage { minigame = this.minigame, id = player.netId });
    }
}
