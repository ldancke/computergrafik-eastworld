﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Every minigame inherits from this abstract class.
 * It provides handlers that can be subscribed to and called.
 */
public abstract class Minigame : ScriptableObject
{
    // Minigame name
    [SerializeField]
	new private string name;
    public string Name
    {
        get { return name; }
        set { name = value; }
    }

    // Minimal amount of players needed to start the minigame
    [SerializeField]
    private int minPlayerCount;
    public int MinPlayerCount
    {
        get { return minPlayerCount; }
        set { minPlayerCount = value; }
    }

    // List of players currently playing the minigame
	private List<Player> players;
    protected List<Player> Players
    {
        get { return players; }
    }

    // Delegates for the minigame
    public delegate void GameStartDel();
    public delegate void GameUpdateDel();
    public delegate void GameStopDel();
    public delegate void PlayerDisconnectedDel(Player player);

    private GameStartDel gameStartHandler;
    public GameStartDel GameStartHandler
    {
        get { return gameStartHandler; }
        protected set { gameStartHandler = value; }
    }

    private GameUpdateDel gameUpdateHandler;
    public GameUpdateDel GameUpdateHandler
    {
        get { return gameUpdateHandler; }
        protected set { gameUpdateHandler = value; }
    }

    private GameStopDel gameStopHandler;
    public GameStopDel GameStopHandler
    {
        get { return gameStopHandler; }
        protected set { gameStopHandler = value; }
    }

    private PlayerDisconnectedDel playerDisconnectedHandler;
    public PlayerDisconnectedDel PlayerDisconnectedHandler
    {
        get { return playerDisconnectedHandler; }
        protected set { playerDisconnectedHandler = value; }
    }

    public Minigame()
    {
        gameStartHandler += SetupMinigame;
    }

    private void SetupMinigame()
    {
        players = MinigameManager.singleton.RegisteredPlayers[this].ToList<Player>();
    }
}
