﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.NetworkSystem;

/*
 * Implementation of the horseshoe throwing game
 * Rules: Every player throws after each other.
 * Point(s) are given after every round to the player with the closest horseshoe to the target.
 * Two points if it is less than one horseshoe length away from the target, one point otherwise.
 * Game is over if one player hits the max score, can be set in inspector.
 */
[CreateAssetMenu(fileName = "MinigameCTF", menuName = "Minigame/MinigameCTF", order = 1)]
public class MinigameCTF : Minigame
{
	public enum Side
	{
		Blue,
		Red
	}

  public static MinigameCTF singleton;

	[SerializeField]
	private GameObject[] flags;
	public GameObject[] Flags
	{
			get { return flags; }
			set { flags = value; }
	}

  [SerializeField]
  private int pointsNeededToWin;
  public int PointsNeededToWin
  {
      get { return pointsNeededToWin; }
      set { pointsNeededToWin = value; }
  }

  public Dictionary<Side, int> Score { get; private set; }
	public Dictionary<Player, Side> Team { get; private set; }
	public Dictionary<Side, GameObject> FlagSpawn { get; private set; }
	public Dictionary<Collider, Player> ColliderOf { get; private set; }

	public MinigameCTF()
	{
		GameStartHandler += GameStart;
		GameUpdateHandler += GameUpdate;
		GameStopHandler += GameStop;
		PlayerDisconnectedHandler += PlayerDisconnected;
	}

	private void BeginLogic()
	{
		FlagSpawn = new Dictionary<Side, GameObject>();
		FlagSpawn.Add(Side.Blue, GameObject.FindWithTag("MinigameCTF_BlueFlagSpawn"));
		FlagSpawn.Add(Side.Red, GameObject.FindWithTag("MinigameCTF_RedFlagSpawn"));

		//FlagSpawn[Side.Blue].transform.position

	}

  private void GameStart()
  {
    Debug.Log("Game starting");
    Score = new Dictionary<Side, int>();
    Team = new Dictionary<Player, Side>();
		ColliderOf = new Dictionary<Collider, Player>();

		if(Players.Count < 2 || Players.Count > 8)
			GameStop();

		Score.Add(Side.Blue, 0);
		Score.Add(Side.Red, 0);

		int half = Players.Count / 2;

    for(int i = 0; i < Players.Count; ++i)
    {
			if(i <= half)
				Team.Add(Players[i], Side.Blue);
			else
				Team.Add(Players[i], Side.Red);

			ColliderOf.Add(Players[i].GetComponent<CapsuleCollider>(), Players[i]);
      //  player.TargetActivateHorseshoeController(player.connectionToClient);
    }

		BeginLogic();
    //Players[0].GetComponent<HorseshoeMinigameController>().TargetAllowThrowing(Players[0].connectionToClient);
  }

  private void GameUpdate()
  {
  }

  private void GameStop()
  {
    Debug.Log("Game stopping");
  }

  private void PlayerDisconnected(Player player)
  {
  }
}
