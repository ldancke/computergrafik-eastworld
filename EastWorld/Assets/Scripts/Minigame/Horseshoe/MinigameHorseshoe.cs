﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.NetworkSystem;

/*
 * Implementation of the horseshoe throwing game
 * Rules: Every player throws after each other.
 * Point(s) are given after every round to the player with the closest horseshoe to the target.
 * Two points if it is less than one horseshoe length away from the target, one point otherwise.
 * Game is over if one player hits the max score, can be set in inspector.
 */
[CreateAssetMenu(fileName = "MinigameHorseshoe", menuName = "Minigame/Horseshoe", order = 1)]
public class MinigameHorseshoe : Minigame
{
    public static MinigameHorseshoe singleton;

    [SerializeField]
    private int pointsNeededToWin;
    public int PointsNeededToWin
    {
        get { return pointsNeededToWin; }
        set { pointsNeededToWin = value; }
    }

    private Transform target;
    public Transform Target
    {
        get { return target; }
        set { target = value; }
    }

    private int turnOffset;
    public int TurnOffset
    {
        get { return turnOffset; }
        set { turnOffset = value; }
    }

    private Dictionary<Player, int> score;
    public Dictionary<Player, int> Score
    {
        get { return score; }
        set { score = value; }
    }

    private Dictionary<Player, GameObject> horseshoes;
    public Dictionary<Player, GameObject> Horseshoes
    {
        get { return horseshoes; }
        set { horseshoes = value; }
    }

    private GameObject lastThrownHorseshoe;
    public GameObject LastThrownHorseshoe
    {
        get { return lastThrownHorseshoe; }
        set { lastThrownHorseshoe = value; }
    }

		public Vector3 LastThrownHorseshoePosition { get; set; }

    private bool gameFinished = false;

    public MinigameHorseshoe()
    {
        singleton = this;
        GameStartHandler += GameStart;
        GameUpdateHandler += GameUpdate;
        GameStopHandler += GameStop;
        PlayerDisconnectedHandler += PlayerDisconnected;
    }

    public void ProceedWithNextPlayer()
    {
        if (turnOffset + 1 >= Players.Count)
            EvaluateRound();
        else
            turnOffset++;

        if (!gameFinished)
        {
            Debug.Log("Allow throwing");
            Players[turnOffset].GetComponent<HorseshoeMinigameController>().TargetAllowThrowing(Players[turnOffset].connectionToClient);
        }

    }

    private float DistanceToTarget(Vector3 position)
    {
        return Vector3.SqrMagnitude(target.position - position);
    }

    private void DestroyHorseshoes()
    {
        List<GameObject> horseshoesObjects = horseshoes.Values.ToList<GameObject>();
        foreach (GameObject horseshoe in horseshoesObjects)
            Destroy(horseshoe);
        horseshoes.Clear();
    }

    private void EvaluateRound()
    {
        Player bestThrower = Players[0];
        for (int i = 1; i < Players.Count; i++)
            if (DistanceToTarget(horseshoes[Players[i]].transform.position) < DistanceToTarget(horseshoes[bestThrower].transform.position))
                bestThrower = Players[i];

        score[bestThrower] = (DistanceToTarget(horseshoes[bestThrower].transform.position) < 0.2) ? score[bestThrower] + 2 : score[bestThrower] + 1;

        DestroyHorseshoes();

        if (score[bestThrower] >= pointsNeededToWin)
        {
            Debug.Log("Game won!");
            gameFinished = true;
            bestThrower.connectionToServer.Send(104, new EmptyMessage());
        } else {
            turnOffset = 0;
        }
    }

		private bool IsGrounded(Transform horseshoe)
		{
			RaycastHit hitInfo;
			if (Physics.Raycast(horseshoe.position, Vector3.down, out hitInfo, 0.1f))
				return true;
			else
				return false;
		}

    private void GameStart()
    {
        Debug.Log("Game starting");
        target = HorseshoeThrowingTarget.singleton.transform;
        turnOffset = 0;
        score = new Dictionary<Player, int>();
        horseshoes = new Dictionary<Player, GameObject>();
        gameFinished = false;

        foreach (Player player in Players)
        {
            score.Add(player, 0);
            player.TargetActivateHorseshoeController(player.connectionToClient);
        }
        Players[0].GetComponent<HorseshoeMinigameController>().TargetAllowThrowing(Players[0].connectionToClient);
    }

    private void GameUpdate()
    {
        if (lastThrownHorseshoe != null)
					if(Vector3.SqrMagnitude(lastThrownHorseshoe.transform.position - LastThrownHorseshoePosition) > 0.1)

            if (lastThrownHorseshoe.GetComponent<Rigidbody>().velocity.sqrMagnitude < 0.1 && IsGrounded(lastThrownHorseshoe.transform))
            {
                lastThrownHorseshoe = null;
                ProceedWithNextPlayer();
            }
    }

    private void GameStop()
    {
        Debug.Log("Game stopping");
        foreach (Player player in Players)
            player.TargetDeactivateHorseshoeController(player.connectionToClient);

    }

    private void PlayerDisconnected(Player player)
    {

    }
}
