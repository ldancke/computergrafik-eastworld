﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Events;

public class NetworkCommunicator : NetworkBehaviour
{
    [Serializable]
    public class MessageEntry
    {
        [Serializable]
        public class MessageEvent : UnityEvent<NetworkMessage> { }

        [SerializeField]
        private short id;
        public short Id
        {
            get { return id; }
            set { id = value; }
        }

        [SerializeField]
        private MessageEvent messageEvents = new MessageEvent();
        public MessageEvent MessageEvents
        {
            get { return messageEvents; }
            set { messageEvents = value; }
        }
    }

    [SerializeField]
    private List<MessageEntry> serverMessageRegistry;
    public List<MessageEntry> ServerMessageRegistry
    {
        get { return serverMessageRegistry; }
        set { serverMessageRegistry = value; }
    }

    [SerializeField]
    private List<MessageEntry> clientMessageRegistry;
    public List<MessageEntry> ClientMessageRegistry
    {
        get { return clientMessageRegistry; }
        set { clientMessageRegistry = value; }
    }

    public void RegisterServerHandler()
    {
        foreach (MessageEntry entry in serverMessageRegistry)
            NetworkServer.RegisterHandler(entry.Id, entry.MessageEvents.Invoke);
    }

    public void UnregisterServerHandler()
    {
        foreach (MessageEntry entry in serverMessageRegistry)
            NetworkServer.UnregisterHandler(entry.Id);
    }

    public override void OnStartServer()
    {
        RegisterServerHandler();
    }

    public void RegisterClientHandler()
    {
        List<NetworkClient> networkClients = NetworkClient.allClients;
        foreach (NetworkClient networkClient in networkClients)
            foreach (MessageEntry entry in clientMessageRegistry)
                networkClient.RegisterHandler(entry.Id, entry.MessageEvents.Invoke);
    }
            

    public void UnregisterClientHandler()
    {
        List<NetworkClient> networkClients = NetworkClient.allClients;
        foreach (NetworkClient networkClient in networkClients)
            foreach (MessageEntry entry in clientMessageRegistry)
                networkClient.UnregisterHandler(entry.Id);
            
    }

    public override void OnStartClient()
    {
        RegisterClientHandler();
    }
}
