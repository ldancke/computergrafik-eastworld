using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class NetworkManagerBrain : NetworkManager
{
	[SerializeField]
	private GameObject[] characters;
	public GameObject[] Characters
	{
			get { return characters; }
			set { characters = value; }
	}

	[SerializeField]
	private CustomSkin customSkinProfile;
	public CustomSkin CustomSkinProfile
	{
			get { return customSkinProfile; }
			set { customSkinProfile = value; }
	}

	private int selectedModel;

	public override void OnClientConnect(NetworkConnection conn)
	{
		if(!base.clientLoadedScene)
		{
			int model = 0;

			if(customSkinProfile.Model == CustomSkin.ModelType.Gunslinger)
				model = 0;
			else if(customSkinProfile.Model == CustomSkin.ModelType.Cowboy)
				model = 1;

			//model = 1;
			IntegerMessage message = new IntegerMessage(model);

			ClientScene.Ready(conn);
			ClientScene.AddPlayer(conn, 0, message);
		}
	}

	public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId, NetworkReader extraMessageReader)
	{
		if (extraMessageReader != null)
		{
			var stream = extraMessageReader.ReadMessage<IntegerMessage> ();
			selectedModel = stream.value;
		}

		GameObject[] spawnPositions = GameObject.FindGameObjectsWithTag("SpawnPoint");
		Vector3 position;

		if(spawnPositions == null || spawnPositions.Length < 1)
		{
			position = Vector3.zero;
		} else {
			Transform spawnPosition = spawnPositions[Random.Range(0, spawnPositions.Length)].GetComponent<Transform>();
			position = spawnPosition.position;
		}

		GameObject playerPrefab = characters[selectedModel];
		GameObject player = Instantiate(playerPrefab, position, Quaternion.identity) as GameObject;
		NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
	}
}
