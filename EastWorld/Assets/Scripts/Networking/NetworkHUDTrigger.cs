﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkHUDTrigger : MonoBehaviour
{
	void OnTriggerEnter(Collider collider)
	{
		if(collider.gameObject.GetComponent<Player>() == null)
			return;

		if(!collider.gameObject.GetComponent<Player>().isLocalPlayer)
			return;

		NetworkManagerHUD.singleton.OpenSetupWindow();
	}

	void OnTriggerExit(Collider collider)
	{
		if(collider.gameObject.GetComponent<Player>() == null)
			return;

		if(!collider.gameObject.GetComponent<Player>().isLocalPlayer)
			return;

		NetworkManagerHUD.singleton.HideWindow();
	}
}
