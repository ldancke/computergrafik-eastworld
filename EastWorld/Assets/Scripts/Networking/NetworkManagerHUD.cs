using System;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class NetworkManagerHUD : MonoBehaviour
{
	public static NetworkManagerHUD singleton;
	public NetworkManager manager;

	[SerializeField]
	private CustomPlayerProfile customProfile;
	public CustomPlayerProfile CustomProfile
	{
		get { return customProfile; }
		set { customProfile = value; }
	}

	[SerializeField]
	private GameObject window;
	public GameObject Window
	{
		get { return window; }
		set { window = value; }
	}

	[SerializeField]
	private GameObject setupWindow;
	public GameObject SetupWindow
	{
		get { return setupWindow; }
		set { setupWindow = value; }
	}

	[SerializeField]
	private InputField setupInputField;
	public InputField SetupInputField
	{
		get { return setupInputField; }
		set { setupInputField = value; }
	}

	[SerializeField]
	private GameObject pendingWindow;
	public GameObject PendingWindow
	{
		get { return pendingWindow; }
		set { pendingWindow = value; }
	}

	[SerializeField]
	private Text pendingText;
	public Text PendingText
	{
		get { return pendingText; }
		set { pendingText = value; }
	}

	public bool IsDirectConnect { get; set; }

	private string pendingTextOrig;

	void InitializeSingleton()
	{
		if(singleton != null && singleton == this)
			return;

		singleton = this;
	}

  void Awake()
  {
		InitializeSingleton();

    manager = GetComponent<NetworkManager>();
		pendingTextOrig = pendingText.text;
  }

	void Start()
	{
		HideWindow();

		IsDirectConnect = customProfile.DirectConnect;

		if(IsDirectConnect)
			DirectConnect(customProfile.DirectConnectAddress);
		else
			StartAsHost();
	}

	bool HasNoConnection()
	{
		return (manager.client == null || manager.client.connection == null || manager.client.connection.connectionId == -1);
	}

	public void StartAsHost()
	{
		manager.StartHost();
	}

	public void HideWindow()
	{
		setupWindow.SetActive(false);
		pendingWindow.SetActive(false);
	}

	public void OnStartClient()
	{
		manager.StopHost();
		manager.StartClient();

		if(HasNoConnection() && !manager.IsClientConnected() && !NetworkServer.active)
		{
			setupWindow.SetActive(false);
			OpenPendingWindow();
		}
	}

	public void OnStopClient()
	{
		manager.StopClient();
		manager.StartHost();

		pendingWindow.SetActive(false);
	}

	public void UpdateNetworkAddress()
	{
		manager.networkAddress = setupInputField.text;
	}

	public void OpenSetupWindow()
	{
		setupWindow.SetActive(true);
	}

	public void OpenPendingWindow()
	{
		pendingWindow.SetActive(true);
		pendingText.text = pendingTextOrig + manager.networkAddress + ":" + manager.networkPort;
	}

	public void DirectConnect(string address)
	{
		manager.networkAddress = address;
		manager.StartClient();
	}
}
