﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPowerBar : MonoBehaviour
{
    public static UIPowerBar singleton;

    [SerializeField]
    private Slider slider;
    public Slider Slider
    {
        get { return slider; }
        set { slider = value; }
    }

    [SerializeField]
    private Image image;
    public Image Image
    {
        get { return image; }
        set { image = value; }
    }

    [SerializeField]
    private float step;
    public float Step
    {
        get { return step; }
        set { step = value; }
    }

    private int direction;
    public int Direction
    {
        get { return direction; }
        set { direction = value; }
    }

    public UIPowerBar()
    {
        singleton = this;
    }

    public void ShowSlider()
    {
        slider.gameObject.SetActive(true);
    }

    public void HideSlider()
    {
        slider.gameObject.SetActive(false);
    }

    public void InstantiateSlider()
    {
        slider.value = slider.minValue;
        direction = 1;
    }

    public void DrawSlider()
    {
        slider.value += step * direction;
        if (slider.value <= slider.minValue || slider.value >= slider.maxValue)
            direction *= -1;
        image.color = Color.Lerp(Color.green, Color.red, slider.value);

    }
}
