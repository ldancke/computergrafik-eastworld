using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Serialization;

public class UILobby : MonoBehaviour
{
	public static UILobby singleton;

  public class UIMessage : MessageBase
  {
    public Minigame minigame;
    public string[] playerNames;

    public override void Serialize(NetworkWriter writer)
    {
      writer.Write(MinigameManager.singleton.MinigameRegistry.IndexOf(minigame));
      writer.Write(playerNames.Length);
      for (int i = 0; i < playerNames.Length; i++)
          writer.Write(playerNames[i]);
    }

    public override void Deserialize(NetworkReader reader)
    {
      minigame = MinigameManager.singleton.MinigameRegistry[reader.ReadInt32()];
      int playerNamesSize = reader.ReadInt32();
      playerNames = new string[playerNamesSize];
      for (int i = 0; i < playerNamesSize; i++)
          playerNames[i] = reader.ReadString();
    }
  }

	[SerializeField]
	private GameObject window;
	public GameObject Window
	{
		get { return window; }
		set { window = value; }
	}

	[SerializeField]
	private Text titleText;
	public Text TitleText
	{
		get { return titleText; }
		set { titleText = value; }
	}

	[SerializeField]
	private GameObject[]  listItems;
	public GameObject[] ListITems
	{
		get { return listItems; }
		set { listItems = value; }
	}

	private Player player;
	private Text[] listItemText;
	private Minigame currentMinigame;

	void InitializeSingleton()
	{
		if(singleton != null && singleton == this)
			return;

		singleton = this;
	}

	public void Awake()
	{
		InitializeSingleton();

		HideLobby();

		listItemText = new Text[listItems.Length];

		for(int i = 0; i < listItemText.Length; ++i)
		{
			listItemText[i] = listItems[i].GetComponentInChildren<Text>();
			listItems[i].SetActive(false);
		}
	}

	public void ShowLobby()
	{
		window.SetActive(true);
	}

	public void HideLobby()
	{
		window.SetActive(false);
	}

	public void AddPlayers(string[] names)
	{
		for(int i = 0; i < names.Length; ++i)
		{
			listItemText[i].text = names[i];
			listItems[i].SetActive(true);
		}
	}

	public void StartMinigame()
	{
		if(player == null)
			player = Player.singleton;

		HideLobby();

		if (player.isServer)
		{
			MinigameManager.singleton.StartMinigame(currentMinigame);
		} else {
			MinigameManager.MinigameMessage message = new MinigameManager.MinigameMessage();
			message.minigame = currentMinigame;
			player.connectionToServer.Send(103, message);
		}
	}

	public void UpdatePlayerName(string playerName)
	{
		if(player == null)
			player = Player.singleton;

		if (player.isLocalPlayer)
				player.CmdSendPlayerName(playerName);
	}

	public void DrawUI(NetworkMessage networkMessage)
	{
		ShowLobby();
		UIMessage message = networkMessage.ReadMessage<UIMessage>();
		currentMinigame = message.minigame;
		titleText.text = currentMinigame.Name;
		AddPlayers(message.playerNames);
	}

	public void TeardownUI(NetworkMessage networkMessage)
	{
		HideLobby();
	}
}
