﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UITitle : MonoBehaviour
{
	[SerializeField]
	private CustomSkin customSkinProfile;
	public CustomSkin CustomSkinProfile
	{
		get { return customSkinProfile; }
		set { customSkinProfile = value; }
	}

	[SerializeField]
	private CustomPlayerProfile customProfile;
	public CustomPlayerProfile CustomProfile
	{
		get { return customProfile; }
		set { customProfile = value; }
	}

	[SerializeField]
	private InputField inputPlayerName;
	public InputField InputPlayerName
	{
		get { return inputPlayerName; }
		set { inputPlayerName = value; }
	}

	[SerializeField]
	private InputField inputIpAddress;
	public InputField InputIpAddress
	{
		get { return inputIpAddress; }
		set { inputIpAddress = value; }
	}

	[SerializeField]
	private Dropdown dropDownModel;
	public Dropdown DropDownModel
	{
		get { return dropDownModel; }
		set { dropDownModel = value; }
	}

	[SerializeField]
	private Dropdown dropDownHat;
	public Dropdown DropDownHat
	{
		get { return dropDownHat; }
		set { dropDownHat = value; }
	}

	public void OnButtonDirectConnect()
	{
		customProfile.PlayerName = inputPlayerName.text;
		customProfile.DirectConnect = true;
		customProfile.DirectConnectAddress = inputIpAddress.text;
		customSkinProfile.Model = (CustomSkin.ModelType) dropDownModel.value;
		customSkinProfile.Hat = (CustomSkin.HatType) dropDownHat.value;

		SceneManager.LoadScene("InGame", LoadSceneMode.Single);
	}

	public void OnButtonPlay()
	{
		customProfile.PlayerName = inputPlayerName.text;
		customProfile.DirectConnect = false;
		customSkinProfile.Model = (CustomSkin.ModelType) dropDownModel.value;
		customSkinProfile.Hat = (CustomSkin.HatType) dropDownHat.value;

		SceneManager.LoadScene("InGame", LoadSceneMode.Single);
	}
}
