using System;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.Serialization;
using Cinemachine;

public class CinemachineTrigger : MonoBehaviour
{
	void OnTriggerEnter(Collider collider)
	{
		if(collider.gameObject.GetComponent<Player>() == null)
			return;

		if(!collider.gameObject.GetComponent<Player>().isLocalPlayer)
			return;

		CinemachineChanger.singleton.Trigger(gameObject);
	}
}
