using System;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.Serialization;
using Cinemachine;

public class CinemachineChanger : MonoBehaviour
{
	[Serializable]
	public class TriggerEvent
	{
		[SerializeField]
		private GameObject trigger;
		public GameObject Trigger
		{
			get { return trigger; }
			set { trigger = value; }
		}

		public bool Triggered { get; set; }
	}

	public static CinemachineChanger singleton;

	[SerializeField]
	private TriggerEvent[] triggerEvents;
	public TriggerEvent[] TriggerEvents
	{
		get { return triggerEvents; }
		set { triggerEvents = value; }
	}

	private ICinemachineCamera freeLookCamera;
	private ICinemachineCamera dollyTrackCamera;

	void InitializeSingleton()
	{
		if(singleton != null && singleton == this)
			return;

		singleton = this;
	}

	void Awake()
	{
		InitializeSingleton();
	}

	void Start()
	{

	}

	void Update()
	{
		if(freeLookCamera == null || dollyTrackCamera == null)
			FindCinemachine();
	}

	private void FindCinemachine()
	{
		if(!Player.IsReady)
			return;

		freeLookCamera = GameObject.FindWithTag(Player.singleton.FreeLook).GetComponent<ICinemachineCamera>();
		dollyTrackCamera = GameObject.FindWithTag(Player.singleton.DollyTrack).GetComponent<ICinemachineCamera>();
	}

	private void CheckCinemachine()
	{
		foreach(TriggerEvent trigger in triggerEvents)
			if(!trigger.Triggered)
				return;

		if(freeLookCamera.Priority > dollyTrackCamera.Priority)
		{
			freeLookCamera.Priority = dollyTrackCamera.Priority;
			dollyTrackCamera.Priority = dollyTrackCamera.Priority + 1;
		} else if(freeLookCamera.Priority < dollyTrackCamera.Priority) {
			dollyTrackCamera.Priority = freeLookCamera.Priority;
			freeLookCamera.Priority = freeLookCamera.Priority+ 1;
		}

		foreach(TriggerEvent trigger in triggerEvents)
			trigger.Triggered = false;
	}

	public void Trigger(GameObject triggered)
	{
		foreach(TriggerEvent trigger in triggerEvents)
			if(trigger.Trigger == triggered)
			{
				trigger.Triggered = true;
				break;
			}

		CheckCinemachine();
	}
}
