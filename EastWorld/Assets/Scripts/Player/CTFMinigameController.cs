using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CTFMinigameController : NetworkBehaviour
{
	[SerializeField]
	private MinigameCTF minigame;
	public MinigameCTF Minigame
	{
			get { return minigame; }
			set { minigame = value; }
	}

	private CharacterUserControl control;
	private bool punching;
	private float stamina;
	private Transform spawnPoint;

	[Command]
	private void CmdSpawnFlags()
	{
		int flagOffset = 0;

		foreach(MinigameCTF.Side side in minigame.FlagSpawn.Keys)
		{
			GameObject instance = Instantiate(minigame.Flags[flagOffset], minigame.FlagSpawn[side].transform.position, Quaternion.identity);
			NetworkServer.Spawn(instance);

			++flagOffset;
		}
	}

	[Command]
	private void CmdPickupFlag(MinigameCTF.Side side)
	{

	}

	[Command]
	private void CmdPunch()
	{

	}

	private void Pickup(Player player, MinigameCTF.Side side)
	{
		CmdPickupFlag(side);
	}

	private void Punch(Player player)
	{
		CharacterMotor.singleton.IsPunching = false;
	}

	private void StartPunching()
	{
		if(punching)
			return;

		RaycastHit hit;

		if(Physics.Raycast(spawnPoint.position, transform.forward, out hit, 4.0f))
		{
			//if(hit.collider == null && !minigame.ColliderOf.ContainsKey(hit.collider))
				//return;

			Player playerHit = minigame.ColliderOf[hit.collider];

			//if(minigame.Team[playerHit] == minigame.Team[Player.singleton])
				//return;

			CharacterMotor.singleton.IsPunching = true;
			CharacterUserControl.singleton.LockAxis = true;
			punching = true;
			Punch(playerHit);
			Invoke("StopPunching", 1f);
		}
	}

	private void StopPunching()
	{
		punching = false;
		CharacterUserControl.singleton.LockAxis = false;
	}

	void Start()
	{
		control = CharacterUserControl.singleton;
		spawnPoint = Player.singleton.SpawnPoint;
	}

	void Update()
	{
		if(control.GetButtonDown("Punching"))
			StartPunching();
	}

	void OnCollisionEnter(Collision collision)
	{
		switch(collision.gameObject.tag)
		{
			case "MinigameCTF_RedFlag":
				Pickup(Player.singleton, MinigameCTF.Side.Red);
				break;
			case "MinigameCTF_BlueFlag":
				Pickup(Player.singleton, MinigameCTF.Side.Blue);
				break;
			default:
				break;
		}

	}

	public void SetupGameSingle()
	{
		CmdSpawnFlags();
	}

	public void SetupGameMulti()
	{
		stamina = 1f;


	}
}
