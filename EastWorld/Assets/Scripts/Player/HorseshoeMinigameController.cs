﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class HorseshoeMinigameController : NetworkBehaviour
{
    [SerializeField]
    private GameObject throwObject;
    public GameObject ThrowObject
    {
        get { return throwObject; }
        set { throwObject = value; }
    }

    private bool throwingAllowed;
    public bool ThrowingAllowed
    {
        get { return throwingAllowed; }
        set { throwingAllowed = value; }
    }

    private float throwingForceMultiplier;
    public float ThrowingForceMultiplier
    {
        get { return throwingForceMultiplier; }
        set { throwingForceMultiplier = value; }
    }

    private Animator playerAnimator;
    private bool throwing;
    private Transform spawnPoint;
    private GameObject instance;
    private CharacterUserControl control;

    void OnEnable()
    {
        CharacterUserControl.singleton.UnlockButton("Throwing");
        CharacterUserControl.singleton.LockButton("Jump");
    }

    void OnDisable()
    {
        CharacterUserControl.singleton.LockButton("Throwing");
        CharacterUserControl.singleton.UnlockButton("Jump");
    }

    private void Awake()
    {
        throwingAllowed = false;
        throwingForceMultiplier = 0.5f;
    }

    private void Start()
    {
        control = CharacterUserControl.singleton;
        playerAnimator = GetComponent<Animator>();
        spawnPoint = Player.singleton.SpawnPoint;
    }

    [Command]
    private void CmdThrow(float force)
    {
        instance = Instantiate(throwObject, spawnPoint.position, Quaternion.identity) as GameObject;
        instance.GetComponent<Rigidbody>().AddForce(transform.forward * 1000 * force);
				//RpcAddForce(instance.tag, transform.forward * 1000 * force);
				MinigameHorseshoe.singleton.LastThrownHorseshoePosition = spawnPoint.position;
        MinigameHorseshoe.singleton.Horseshoes.Add(GetComponent<Player>(), instance);
        MinigameHorseshoe.singleton.LastThrownHorseshoe = instance;
				NetworkServer.Spawn(instance);
    }

		[ClientRpc]
		private void RpcAddForce(string instanceOf, Vector3 force)
		{
			GameObject.FindWithTag(instanceOf).GetComponent<Rigidbody>().AddForce(force);
		}

    private void StartThrowing()
    {
        if (!throwingAllowed)
            return;

        throwingAllowed = false; // Added hack
        CharacterUserControl.singleton.LockAxis = true;
        throwing = true;
        Invoke("Throw", 3f);
        CharacterMotor.singleton.IsThrowing = true;
        Invoke("StopThrowing", 4f);
    }

		[Client]
    private void Throw()
    {
        if (!throwing)
            return;

        CmdThrow(ThrowingForceMultiplier);
        throwingAllowed = false;
        UIYourTurn.singleton.gameObject.SetActive(false);
        CharacterMotor.singleton.IsThrowing = false;
    }

    private void StopThrowing()
    {
        throwing = false;
        CharacterUserControl.singleton.LockAxis = false;
    }

    [TargetRpc]
    public void TargetAllowThrowing(NetworkConnection connection)
    {
        throwingAllowed = true;
        UIYourTurn.singleton.gameObject.SetActive(true);
    }

    bool buttonWasPressed;
    private void Update()
    {
        if (!throwingAllowed)
            return;

        if (Input.GetButton("Throwing"))
        {
            if (!buttonWasPressed)
            {
                buttonWasPressed = true;
                UIPowerBar.singleton.InstantiateSlider();
                UIPowerBar.singleton.ShowSlider();
            }
            UIPowerBar.singleton.DrawSlider();
        } else {
            if (buttonWasPressed)
            {
                buttonWasPressed = false;
                throwingForceMultiplier = UIPowerBar.singleton.Slider.value + 0.1f;
                UIPowerBar.singleton.HideSlider();
                StartThrowing();
            }
        }

        /*if (control.GetButtonDown("Throwing"))
            StartThrowing();*/
    }
}
