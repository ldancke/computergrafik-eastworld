using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Cinemachine;

public class Player : NetworkBehaviour
{
	public static Player singleton;
	public static bool IsReady;

	[SerializeField]
	private CustomPlayerProfile customProfile;
	public CustomPlayerProfile CustomProfile
	{
		get { return customProfile; }
		set { customProfile = value; }
	}

	[SerializeField]
	private CustomSkin customSkinProfile;
	public CustomSkin CustomSkinProfile
	{
		get { return customSkinProfile; }
		set { customSkinProfile = value; }
	}

	[SerializeField]
	private GameObject goodGuy;
	public GameObject GoodGuy
	{
		get { return goodGuy; }
		set { goodGuy = value; }
	}

	[SerializeField]
	private GameObject badGuy;
	public GameObject BadGuy
	{
		get { return badGuy; }
		set { badGuy = value; }
	}

	[SerializeField]
	private string freeLook;
	public string FreeLook
	{
		get { return freeLook; }
		set { freeLook = value; }
	}

	[SerializeField]
	private string dollyTrack;
	public string DollyTrack
	{
		get { return dollyTrack; }
		set { dollyTrack = value; }
	}

	[SerializeField]
	private Transform spawnPoint;
	public Transform SpawnPoint
	{
		get { return spawnPoint; }
		set { spawnPoint = value; }
	}

  [SyncVar]
  private string playerName;
  public string PlayerName
  {
    get { return playerName; }
    set { playerName = value; }
  }

	public bool IsDirectConnect { get; set; }

	private ICinemachineCamera freeLookCamera;
	private ICinemachineCamera dollyTrackCamera;

	void InitializeSingleton()
	{
		if(singleton != null && singleton == this && !isLocalPlayer)
			return;

		singleton = this;
		IsReady = true;
	}

	public void Start()
	{
		InitializeSingleton();

		FindHat();
		CmdSendPlayerName(customProfile.PlayerName);
	}

	public void FindHat()
	{
		switch(customSkinProfile.Hat)
		{
			case CustomSkin.HatType.GoodGuy:
				goodGuy.SetActive(true);
				goodGuy.SetActive(true);
				break;
			case CustomSkin.HatType.BadGuy:
				badGuy.SetActive(false);
				goodGuy.SetActive(true);
				break;
		}
	}

  public override void OnStartLocalPlayer()
  {
		freeLookCamera = GameObject.FindWithTag(freeLook).GetComponent<ICinemachineCamera>();
		dollyTrackCamera = GameObject.FindWithTag(dollyTrack).GetComponent<ICinemachineCamera>();

		freeLookCamera.LookAt = GetComponent<Transform>();
		freeLookCamera.Follow = GetComponent<Transform>();

		dollyTrackCamera.LookAt = GetComponent<Transform>();
		dollyTrackCamera.Follow = GetComponent<Transform>();
  }

  [Command]
  public void CmdSendPlayerName(string value)
  {
      playerName = value;
  }

  [TargetRpc]
  public void TargetActivateHorseshoeController(NetworkConnection connection)
  {
      GetComponent<HorseshoeMinigameController>().enabled = true;
  }

  [TargetRpc]
  public void TargetDeactivateHorseshoeController(NetworkConnection connection)
  {
      GetComponent<HorseshoeMinigameController>().enabled = false;
  }
}
