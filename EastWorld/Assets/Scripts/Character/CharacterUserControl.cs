using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

[RequireComponent(typeof(CharacterMotor))]
public class CharacterUserControl : MonoBehaviour
{
	public static CharacterUserControl singleton;

	[Serializable]
	public class MouseEvent : UnityEvent {}

	[SerializeField]
	private bool mouseActive;
	public bool MouseActive
	{
		get { return mouseActive; }
		set { mouseActive = value; }
	}

	[SerializeField]
	private MouseEvent mouseEvents;
	public MouseEvent MouseEvents
	{
		get { return mouseEvents; }
		set { mouseEvents = value; }
	}

	public bool ForceWalk { get; set; }
	public bool LockAxis { get; set; }

	private List<string> lockedButtons;
	private CharacterMotor characterMotor;
	private Transform mainCamera;
	private Vector3 cameraForward;
	private Vector3 move;
	private bool jump;

	void InitializeSingleton()
	{
		if(singleton != null && singleton == this)
			return;

		singleton = this;
	}

	public void Awake()
	{
		InitializeSingleton();

		lockedButtons = new List<string>();
	}

	void Start()
	{
		if(!Player.singleton.isLocalPlayer)
			Destroy(GetComponent<CharacterUserControl>());

		if (Camera.main != null)
		{
				mainCamera = Camera.main.transform;
		}

		characterMotor = GetComponent<CharacterMotor>();
		LockButton("Throwing");
		LockButton("Punching");
	}

	void Update()
	{
		if(!jump)
			jump = false; //GetButtonDown("Jump");

		if(Input.GetMouseButtonDown(0))
			mouseEvents.Invoke();

		if(!characterMotor.IsDead)
			if(Input.GetKeyDown("k"))
				characterMotor.Kill();
	}

	void FixedUpdate()
	{
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");

		if(LockAxis)
		{
			h = 0;
			v = 0;
		}

		if (mainCamera != null)
		{
				cameraForward = Vector3.Scale(mainCamera.forward, new Vector3(1, 0, 1)).normalized;
				move = v * cameraForward + h * mainCamera.right;
		} else {
				move = v * Vector3.forward + h * Vector3.right;
		}

		if (Input.GetButton("Walk") || ForceWalk)
			move = move * 0.5f;

		characterMotor.Move(move, jump);
		jump = false;
	}

	public void LockButton(string button)
	{
		if(lockedButtons.Contains(button))
			return;

		lockedButtons.Add(button);
	}

	public void UnlockButton(string button)
	{
		if(lockedButtons.Contains(button))
			lockedButtons.Remove(button);
	}

	public bool GetButtonDown(string button)
	{
		bool pressed = Input.GetButtonDown(button);

		if(lockedButtons.Contains(button))
			return false;

		return pressed;
	}
}
