using System;
using UnityEngine;
using UnityEngine.Serialization;

public class CharacterMotor : MonoBehaviour
{
	public static CharacterMotor singleton;

	[SerializeField]
	private bool isDead;
	public bool IsDead
	{
		get { return isDead; }
		set {}
	}

	[Range(0f, 3f)]
	[SerializeField]
	private float fadeOutTime;
	public float FadeOutTime
	{
		get { return fadeOutTime; }
		set { fadeOutTime = value; }
	}

	[SerializeField]
	private float movingTurnSpeed;
	public float MovingTurnSpeed
	{
		get { return movingTurnSpeed; }
		set { movingTurnSpeed = value; }
	}

	[SerializeField]
	private float stationaryTurnSpeed;
	public float StationaryTurnSpeed
	{
		get { return stationaryTurnSpeed; }
		set { stationaryTurnSpeed = value; }
	}

	[Range(0f, 20f)]
	[SerializeField]
	private float jumpPower;
	public float JumpPower
	{
		get { return jumpPower; }
		set { jumpPower = value; }
	}

	[Range(1f, 4f)]
	[SerializeField]
	private float gravityMultiplier;
	public float GravityMultiplier
	{
		get { return gravityMultiplier; }
		set { gravityMultiplier = value; }
	}

	[SerializeField]
	private float runCycleLegOffset;
	public float RunCycleLegOffset
	{
		get { return runCycleLegOffset; }
		set { runCycleLegOffset = value; }
	}

	[SerializeField]
	private float moveSpeedMultiplier;
	public float MoveSpeedMultiplier
	{
		get { return moveSpeedMultiplier; }
		set { moveSpeedMultiplier = value; }
	}

	[SerializeField]
	private float animSpeedMultiplier;
	public float AnimSpeedMultiplier
	{
		get { return animSpeedMultiplier; }
		set { animSpeedMultiplier = value; }
	}

	[SerializeField]
	private float groundCheckDistance;
	public float GroundCheckDistance
	{
		get { return groundCheckDistance; }
		set { groundCheckDistance = value; }
	}

	public bool IsThrowing { get; set; }
	public bool IsPunching { get; set; }

	private const float HALF = 0.5f;

	private Transform motorTransform;
	private Rigidbody motorRigidbody;
	private Animator motorAnimator;
	private bool isGrounded;
	private float origGroundCheckDistance;
	private float turnAmount;
	private float forwardAmount;
	private Vector3 groundNormal;
	private float capsuleHeight;
	private Vector3 capsuleCenter;
	private CapsuleCollider capsule;

	void InitializeSingleton()
	{
		if(singleton != null && singleton == this)
			return;

		singleton = this;
	}

	void Awake()
	{
		InitializeSingleton();
		motorAnimator = GetComponent<Animator>();
		motorTransform = GetComponent<Transform>();
		motorRigidbody = GetComponent<Rigidbody>();
		capsule = GetComponent<CapsuleCollider>();
	}

	void Start()
	{
		capsuleHeight = capsule.height;
		capsuleCenter = capsule.center;

		motorRigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
		origGroundCheckDistance = groundCheckDistance;
	}

	void UpdateAnimator(Vector3 move)
	{
		if(IsThrowing || IsPunching)
		{
			motorAnimator.SetBool("Throwing", IsThrowing);
			motorAnimator.SetBool("Punching", IsPunching);
			return;
		}

		motorAnimator.SetFloat("Forward", forwardAmount, 0.1f, Time.deltaTime);
		motorAnimator.SetFloat("Turn", turnAmount, 0.1f, Time.deltaTime);
		motorAnimator.SetBool("OnGround", isGrounded);
		motorAnimator.SetBool("Dead", isDead);
		motorAnimator.SetBool("Throwing", IsThrowing);
		motorAnimator.SetBool("Punching", IsPunching);

		if(isDead)
			return;

		if (!isGrounded)
		{
			motorAnimator.SetFloat("Jump", motorRigidbody.velocity.y);
		}

		float runCycle = Mathf.Repeat(motorAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime + runCycleLegOffset, 1);
		float jumpLeg = (runCycle < HALF ? 1 : -1) * forwardAmount;

		if (isGrounded)
		{
			motorAnimator.SetFloat("JumpLeg", jumpLeg);

			if(move.magnitude > 0)
				motorAnimator.speed = animSpeedMultiplier;
			else
				motorAnimator.speed = 1;
		}
	}

	void HandleAirborneMovement()
	{
		Vector3 extraGravityForce = (Physics.gravity * gravityMultiplier) - Physics.gravity;
		motorRigidbody.AddForce(extraGravityForce);

		groundCheckDistance = motorRigidbody.velocity.y < 0 ? origGroundCheckDistance : 0.01f;
	}

	void HandleGroundedMovement(bool jump)
	{
		if (jump && motorAnimator.GetCurrentAnimatorStateInfo(0).IsName("Grounded"))
		{
			motorRigidbody.velocity = new Vector3(motorRigidbody.velocity.x, jumpPower, motorRigidbody.velocity.z);
			isGrounded = false;
			motorAnimator.applyRootMotion = false;
			groundCheckDistance = 0.1f;
		}
	}

	void ApplyExtraTurnRotation()
	{
		float turnSpeed = Mathf.Lerp(stationaryTurnSpeed, movingTurnSpeed, forwardAmount);
		transform.Rotate(0, turnAmount * turnSpeed * Time.deltaTime, 0);
	}

	public void OnAnimatorMove()
	{
		if (isGrounded && Time.deltaTime > 0)
		{
			Vector3 v = (motorAnimator.deltaPosition * moveSpeedMultiplier) / Time.deltaTime;
			v.y = motorRigidbody.velocity.y;
			motorRigidbody.velocity = v;
		}
	}

	void CheckGroundStatus()
	{
		RaycastHit hitInfo;
		if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, groundCheckDistance))
		{
			groundNormal = hitInfo.normal;
			isGrounded = true;
			motorAnimator.applyRootMotion = true;
		} else {
			isGrounded = false;
			groundNormal = Vector3.up;
			motorAnimator.applyRootMotion = false;
		}
	}

	public void Move(Vector3 move, bool jump)
	{
		if (move.magnitude > 1f)
			move.Normalize();

		move = transform.InverseTransformDirection(move);
		CheckGroundStatus();
		move = Vector3.ProjectOnPlane(move, groundNormal);
		turnAmount = Mathf.Atan2(move.x, move.z);

		forwardAmount = move.z;

		if(isDead || IsThrowing)
		{
			UpdateAnimator(move);
			return;
		}

		ApplyExtraTurnRotation();

		if (isGrounded)
			HandleGroundedMovement(jump);
		else
			HandleAirborneMovement();

		UpdateAnimator(move);
	}

	public void Reset(Vector3 position)
	{
		motorTransform.position = position;
		isDead = false;
		Move(Vector3.zero, false);
	}

	public void Kill()
	{
		isDead = true;
	}
}
