using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = "CustomPlayerProfile", menuName = "Customization/PlayerProfile", order = 1)]
public class CustomPlayerProfile : ScriptableObject
{
	[SerializeField]
	private bool directConnect;
	public bool DirectConnect
	{
		get { return directConnect; }
		set { directConnect = value; }
	}

	[SerializeField]
	private string directConnectAddress;
	public string DirectConnectAddress
	{
		get { return directConnectAddress; }
		set { directConnectAddress = value; }
	}

	[SerializeField]
	private string playerName;
	public string PlayerName
	{
		get { return playerName; }
		set { playerName = value; }
	}
}
