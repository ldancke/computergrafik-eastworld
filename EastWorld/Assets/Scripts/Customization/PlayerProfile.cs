using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProfile : MonoBehaviour
{
	[SerializeField]
	private CustomPlayerProfile customProfile;
	public CustomPlayerProfile CustomProfile
	{
		get { return customProfile; }
		set { customProfile = value; }
	}

	public void SetPlayerName(string name)
	{
		customProfile.PlayerName = name;
	}
}
