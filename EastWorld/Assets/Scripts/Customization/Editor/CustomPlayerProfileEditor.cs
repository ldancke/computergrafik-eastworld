using UnityEngine;
using System.Collections;
using UnityEditor;

public class CustomPlayerProfileEditor
{
    [MenuItem("Assets/Create/CustomPlayerProfile")]
    public static void CreateMyAsset()
    {
        CustomSkin asset = ScriptableObject.CreateInstance<CustomSkin>();

        AssetDatabase.CreateAsset(asset, "Assets/CustomSkin.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}
