using UnityEngine;
using System.Collections;
using UnityEditor;

public class CustomSkinEditor
{
    [MenuItem("Assets/Create/CustomSkin")]
    public static void CreateMyAsset()
    {
        CustomSkin asset = ScriptableObject.CreateInstance<CustomSkin>();

        AssetDatabase.CreateAsset(asset, "Assets/CustomSkin.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}
