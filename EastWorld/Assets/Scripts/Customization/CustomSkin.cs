using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = "CustomSkin", menuName = "Customization/Skin", order = 1)]
public class CustomSkin : ScriptableObject
{
	[Serializable]
	public enum HatType
	{
		BadGuy,
		GoodGuy
	}

	[Serializable]
	public enum ModelType
	{
		Gunslinger,
		Cowboy
	}

	[SerializeField]
	private HatType hat;
	public HatType Hat
	{
		get { return hat; }
		set { hat = value; }
	}

	[SerializeField]
	private ModelType model;
	public ModelType Model
	{
		get { return model; }
		set { model = value; }
	}

	public string HatAsTag()
	{
		return "Hat_" + hat.ToString();
	}
}
